function obtenerHora(){

    let fecha = new Date();
    // console.log(fecha);
    // console.log(fecha.getDate());
    // console.log(fecha.getDay());
    // console.log(fecha.getMonth());

    let pDiaSemana = document.getElementById('diaSemana'),
    pDia = document.getElementById('dia'),
    pMes = document.getElementById('mes'),
    pAnio = document.getElementById('anio'),
    pHoras = document.getElementById('horas'),
    pMinutos = document.getElementById('minutos'),
    pSegundos = document.getElementById('segundos'),
    pAmPm = document.getElementById('ampm');

    let diasReloj = ["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];

    let meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

    pDiaSemana.innerText = diasReloj[fecha.getDay()];
    pDia.innerText = fecha.getDate();
    pMes.innerText = meses[fecha.getMonth()];
    pAnio.innerText = fecha.getFullYear();

    pHoras.innerText = fecha.getHours();
    // pMinutos.innerText = fecha.getMinutes();
    // pSegundos.innerText = fecha.getSeconds();

    if(fecha.getHours() <= 12){
        pAmPm.innerText = "Am";
        pHoras.innerText = fecha.getHours();
    } else {
        pHoras.innerText = fecha.getHours() - 12;
        pAmPm.innerText = "Pm";
    }

    if(parseInt(pHoras.innerText)<10){
        pHoras.innerText = "0"+ pHoras.innerText;
    }

    if(fecha.getMinutes() <10){
        pMinutos.innerText = "0" + fecha.getMinutes();
    } else {
        pMinutos.innerText = fecha.getMinutes();
    }
    
    if(fecha.getSeconds() <10){
        pSegundos.innerText = "0" + fecha.getSeconds();
    } else {
        pSegundos.innerText = fecha.getSeconds();
    }
}


window.setInterval(obtenerHora,1000);
// obtenerHora();